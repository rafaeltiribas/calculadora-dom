//Adicionar Nota
let nNotas = 1
let totalNotas = []
function adicionarNota(){
    let input = document.querySelector(".nota");
    let escopo = document.querySelector(".notas");
    let texto = document.createElement("p");
    texto.innerText = input.value;
    escopo.append(texto);
    totalNotas.push(texto);
    nNotas++;
}

let btn_adicionar = document.querySelector("#adiciona");
btn_adicionar.addEventListener("click", ()=>{adicionarNota()});

//Calcula Media
function calculaMedia(){
    let media = document.querySelector("#media");
    let mediaTotal = 0;
    for(var i=0; i<totalNotas; i++){
        mediaTotal += totalNotas[i];
    }
    mediaTotal = mediaTotal/nNotas;
    media.innerText = mediaTotal
}

let btn_calcula = document.querySelector("#calcula");
btn_calcula.addEventListener("click", ()=>{calculaMedia()});